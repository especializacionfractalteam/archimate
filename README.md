# Ejercicio Arquitectura con Archimate

## Modelo Organizacional
### Diagrama de casos de uso ###
![Alt text](ImagenesModelos/OrganizationViewPoint.png)

## Modelo de Funciones de Negocio
![Alt text](ImagenesModelos/BusinessFunctionViewpoint.jpeg)

## Modelo de Procesos de Negocio
![Alt text](ImagenesModelos/BussinesProcessViewPoint.png)

## Modelo de Estructura de la Aplicación
![Alt text](ImagenesModelos/ApplicationStructureViewpoint.jpeg)

## Modelo de Uso de la Aplicación
![Alt text](ImagenesModelos/ApplicationUsageViewpoint.png)

##

## Modelo de Capas
A diferencia de UML, Archimate nos permite tener un punto de vista más general y fácil de entender, Sin dejar a un lado ningún nivel de abstracción. Para el caso del punto de vista en capas (Layered Viewpoint). Nos permite modelar en un solo diagrama los aspectos de un sistema que van desde los actores pasando por procesos, servicios, componentes de aplicación hasta infraestructura de una forma más ligera y fácil de entender que su equivalente en otros modelados. En un mismo diagrama puedo representar varios servicios y procesos que a su vez pueden o no interactuar con uno u otro actor del sistema. 
Cada capad de este diagrama se relaciona de alguna forma con su antecesora y la capa siguiente. Representa también el flujo que presenta el sistema a través de todas sus capas hasta llegar a su infraestructura. No solo se concentra en el software si no que no también representa el hardware que interviene en el sistema como lo son los servidores también su función a nivel de protocolos, mensajes, NAS, DNS, etc. Ayuda a entender la tecnología que puede estar implicada en la funcionalidad de un sistema.

## Modelo Landscape Map
El modelo Landscape Map permite desde un diagrama compacto, mostrar la relación de las diferentes partes de nuestra arquitectura como las funciones de negocio, componentes de la aplicación y productos, de una forma resumida con los principales conceptos y componentes. 
La forma en como se diagrama permite que para entenderlo no se necesiten conceptos tecnicos ni especializados en cuanto a arquitectura ni ingenieria por lo que este modelo permitiria dar una introducción a la arquitectura propuesta a quienes apenas se integren en el proyecto o en el negocio. 

### Mateo Puerto - 20201099044
### Diego Izquierdo - 20201099035